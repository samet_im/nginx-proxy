# nginx proxy
[![pipeline status](/badges/master/pipeline.svg)](/commits/master)


## Demo Architecture
![](./doc/regexp_proxy/images/arch_solution.png)


## Append header
- [Add request header proxy request](./doc/append_header/README-en.md#add-request-header-proxy-request)
	- [Test environment](./doc/append_header/README-en.md#test-environment)
	- [nginx config](./doc/append_header/README-en.md#nginx-config)
		- [docker command](./doc/append_header/README-en.md#docker-command)
		- [proxy config](./doc/append_header/README-en.md#proxy-config)
	- [Golang server](./doc/append_header/README-en.md#golang-server)
	- [Result](./doc/append_header/README-en.md#result)

## RegExp proxy
- [proxy_pass with regular expression](./doc/regexp_proxy/README-en.md#proxy_pass-with-regular-expression)
  - [Test environment](./doc/regexp_proxy/README-en.md#test-environment)
  - [host config](./doc/regexp_proxy/README-en.md#host-config)
  - [Golang servers](./doc/regexp_proxy/README-en.md#golang-servers)
  - [nginx config](./doc/regexp_proxy/README-en.md#nginx-config)
    - [nginx Dockerfile](./doc/regexp_proxy/README-en.md#nginx-dockerfile)
    - [./conf/default.conf](./doc/regexp_proxy/README-en.md#confdefaultconf)
  - [Result](./doc/regexp_proxy/README-en.md#result)
    - [location ~^\/server-01$](./doc/regexp_proxy/README-en.md#location-server-01)
    - [location ~^\/server-02$](./doc/regexp_proxy/README-en.md#location-server-02)
    - [location ~^\/api\/v1\/server-01\/(?<path>.*)$](./doc/regexp_proxy/README-en.md#location-apiv1server-01path)
    - [location ~^\/api\/v1\/server-02\/(?<path>.*)$](./doc/regexp_proxy/README-en.md#location-apiv1server-02path)
    - [location ~^\/api\/v2\/server-(?<serverId>\d\d)\/(?<path>.*)$](./doc/regexp_proxy/README-en.md#location-apiv2server-serveridddpath)


