# Add request header proxy request
> The code of the solution has been changed. So this documentation may not be up-to-date.


- [Add request header proxy request](#add-request-header-proxy-request)
	- [Test environment](#test-environment)
	- [nginx config](#nginx-config)
		- [docker command](#docker-command)
		- [proxy config](#proxy-config)
	- [Golang server](#golang-server)
	- [Result](#result)


## Test environment
![](./images/arch_solution.png)


The test environment is composed of 3 parts:
- the client Postman: Simulate client request.
- golang server: Echo request headers. It works on port `8888`
- nginx: Configured to pass the request to golang server and append custom header `X-test-header : test-value` to request object. It works on port `8080`

## nginx config
### docker command
- I used docker image to run nginx. To be able to connect to golang server on host, I used `--net=host` to connect directly to host port.
- The default port `80` for nginx was changed to `8080` to avoid host conflict.


The command used to run nginx container: 
``` bash
[ ]$ podman run -v $PWD/conf/default.conf:/etc/nginx/conf.d/default.conf:z -p 8080:8080 --net=host --name nginx --rm nginx
```

### proxy config
`include /etc/nginx/conf.d/*.conf;` inside the default config file `/etc/nginx/nginx.conf` will load the config file `/etc/nginx/conf.d/default.conf`.
I used the following config to:
1. pass the request to golang server: `proxy_pass http://127.0.0.1:8888;`
2. add custom header to the request: `proxy_set_header X-test-header test-value;`
``` bash
[ ]$ cat default.conf 
server {
    listen 8080;

    location / {
        proxy_set_header X-test-header test-value;
        proxy_pass http://127.0.0.1:8888;
    }
}
```

Note that:
- redirection will just return `HTTP/1.1 302 Moved Temporarily` or `HTTP/1.1 301 Moved Permanently` response.
- `add_header X-test-header test-value;` will just add header to response object

## Golang server
I used the following code to run golang server:
``` golang
package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"
)

const (
	// Host name of the HTTP Server
	Host = "0.0.0.0"
	// Port of the HTTP Server
	Port = "8888"
)

func echoHeaders(w http.ResponseWriter, r *http.Request) {
	for k, v := range r.Header.Clone() {
		l := fmt.Sprintf("%-30s", k) + ":\t" + strings.Join(v, ",") + "\n"
		fmt.Fprintf(w, l)
	}
}

func main() {
	http.HandleFunc("/", echoHeaders)
	err := http.ListenAndServe(Host+":"+Port, nil)
	if err != nil {
		log.Fatal("Error Starting the HTTP Server :", err)
		return
	}
}
```

## Result
The following image shows the result of a direct request to golang server on port `8888`:

![](./images/postman_request_direct.png)


A request from postman or a web browser to nginx on port `8080` will return a similar result:

![](./images/postman_request_nginx.png)

