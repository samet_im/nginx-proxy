# Ajout d'un Header personnalisé dans le requête
> Le code de la solution a été changé. Le code de cette documentation n'est plus à jour avec le code de la solution

- [Ajout d'un Header personnalisé dans le requête](#ajout-dun-header-personnalisé-dans-le-requête)
	- [Environnement de test](#environnement-de-test)
	- [Golang server](#golang-server)
		- [Code](#code)
		- [Résultat](#résultat)
	- [Config de nginx](#config-de-nginx)
		- [Commande Docker](#commande-docker)
		- [Explication des configuration de proxy](#explication-des-configuration-de-proxy)
		- [Résultat](#résultat-1)


## Environnement de test
![](./images/arch_solution.png)

L'environnement est composé de 3 parties:
| nom | role | port |
|---|---|---|
| postman | Simuler les requêtes des utilisateurs. |  |
| serveur golang | Écho le header des requêtes issues: | `8888` |
| serveur nginx | Joue le rôle d'un proxy. Il "transfert" les requêtes vers le serveur golang et ajoute un header personnalisé `X-test-header : test-value`.  | `8080` |

- Les serveurs golang et nginx éxucter sous forme des conteneurs Docker. Pour faciliter la communication avec les conteneurs, j'ai utilisé le résau du hôte avec l'option `--net=host`.
- Pour gérer les conteneur, j'ai utiliser l'outil `podman` qui supporte toute les commandes `docker`.

## Golang server
### Code
Pour exécuter le serveur golang, j'ai utilisé l'image docker `docker.io/docker.io/library/golang:1.15`

J'ai utilisé le code suivant pour le serveur golang:
``` golang
package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"
)

const (
	// Host name of the HTTP Server
	Host = "0.0.0.0"
	// Port of the HTTP Server
	Port = "8888"
)

func echoHeaders(w http.ResponseWriter, r *http.Request) {
	for k, v := range r.Header.Clone() {
		l := fmt.Sprintf("%-30s", k) + ":\t" + strings.Join(v, ",") + "\n"
		fmt.Fprintf(w, l)
	}
}

func main() {
	http.HandleFunc("/", echoHeaders)
	err := http.ListenAndServe(Host+":"+Port, nil)
	if err != nil {
		log.Fatal("Error Starting the HTTP Server :", err)
		return
	}
}
```

### Résultat
La capture d'écran suivant montre le résultat d'une requête directe vers le serveur golang sur le port `8888`.

![](./images/postman_request_direct.png)


## Config de nginx
### Commande Docker
Le port par défaut pour nginx `80` est changé vers le port `8080` pour éviter les conflits avec le réseau de la machine hôte.

La commande utilisé pour exécuter le conteneur nginx:
``` bash
[ ]$ podman run -v $PWD/conf/default.conf:/etc/nginx/conf.d/default.conf:z -p 8080:8080 --net=host --name nginx --rm nginx
```

### Explication des configuration de proxy

``` bash
[ ]$ cat default.conf 
server {
    listen 8080;

    location / {
        proxy_set_header X-test-header test-value;
        proxy_pass http://127.0.0.1:8888;
    }
}
```

| config | fichier | role | 
|---|---|---|
| `include /etc/nginx/conf.d/*.conf;` | Fichier de configuration principal `/etc/nginx/nginx.conf` | charger le fichier de configurations  `/etc/nginx/conf.d/default.conf` |
| `proxy_pass http://127.0.0.1:8888;` | `/etc/nginx/conf.d/default.conf` | "transfert" les requêtes vers le serveur golang | 
| `proxy_set_header X-test-header test-value;` | `/etc/nginx/conf.d/default.conf` | Ajout un header `X-test-header test-value` au requêtes | 

Il faut noter que:
1. "transférer" c'est pas "rediriger" ou "redirection". La redirection va retourner une réponse directe au client de type: `HTTP/1.1 302 Moved Temporarily` ou `HTTP/1.1 301 Moved Permanently`.
2. `add_header X-test-header test-value;` va juste ajouter le header dans la reponse et pas dans la requête.


### Résultat
La capture d'écran suivant montre le résultat d'une requête de postman vers le serveur web nginx sur le port `8080`:

![](./images/postman_request_nginx.png)

