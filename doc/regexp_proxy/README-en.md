# proxy_pass with regular expression
> The code of the solution has been changed. So this documentation may not be up-to-date.

- [proxy_pass with regular expression](#proxy_pass-with-regular-expression)
  - [Test environment](#test-environment)
  - [host config](#host-config)
  - [Golang servers](#golang-servers)
  - [nginx config](#nginx-config)
    - [nginx Dockerfile](#nginx-dockerfile)
    - [./conf/default.conf](#confdefaultconf)
  - [Result](#result)
    - [location ~^\/server-01$](#location-server-01)
    - [location ~^\/server-02$](#location-server-02)
    - [location ~^\/api\/v1\/server-01\/(?<path>.*)$](#location-apiv1server-01path)
    - [location ~^\/api\/v1\/server-02\/(?<path>.*)$](#location-apiv1server-02path)
    - [location ~^\/api\/v2\/server-(?<serverId>\d\d)\/(?<path>.*)$](#location-apiv2server-serveridddpath)

## Test environment
![](./images/arch_solution.png)

The test environment is composed of 3 parts:
- the client Postman: Simulate client request.
- golang servers echo request headers:
  - server 1: works on port `8801` and host `local-server-01`
  - server 2: works on port `8802` and host `local-server-02`
- nginx: Configured to pass the request to golang servers and append custom header `X-test-header : test-value` to request object. It works on port `8080`

I used podman as container runtime. It supports all docker commands and does not require root privileges.

## host config
``` bash
[ ]$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

127.0.0.1 local-server-01
127.0.0.1 local-server-02
```

## Golang servers

I used the following code to run golang server:
[golang/main.go](../../golang/main.go)

To run golang server, I used `docker.io/docker.io/library/golang:1.15`
For server 1:
``` bash
[ ]$ export HOST=local-server-01
[ ]$ export PORT=8801
[ ]$ podman run --net=host --env=PORT=$PORT --env=HOST=$HOST -it -v $PWD/golang/main.go:/go/src/app/main.go:z --name=go-server-$PORT --rm golang:1.15 go run /go/src/app/main.go
2020/12/22 12:33:15 server listen on http://c:8801/
```
For server 2:
``` bash
[ ]$ export HOST=local-server-02
[ ]$ export PORT=8802
[ ]$ podman run --net=host --env=PORT=$PORT --env=HOST=$HOST -it -v $PWD/golang/main.go:/go/src/app/main.go:z --name=go-server-$PORT --rm golang:1.15 go run /go/src/app/main.go
2020/12/22 12:35:47 server listen on http://local-server-02:8802/
```


## nginx config
### nginx Dockerfile
I used the following Dockerfile to create nginx image:
```
[ ]$ cat Dockerfile 
FROM nginx:1.19.6

RUN echo '127.0.0.1 local-server-01' >> /etc/hosts
RUN echo '127.0.0.1 local-server-02' >> /etc/hosts

COPY ./conf/default.conf /etc/nginx/conf.d
[ ]$ podman build -f Dockerfile -t nginx-header .
STEP 1: FROM nginx:1.19.6
...
[ ]$ podman run --net=host --name nginx --rm nginx-header
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
10-listen-on-ipv6-by-default.sh: info: /etc/nginx/conf.d/default.conf differs from the packaged version
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
```

### ./conf/default.conf
`./conf/default.conf` content:
``` conf
[ ]$ cat conf/default.conf
server {
    listen 8080;

    location ~^\/server-01$ {
        proxy_set_header X-test-header test-value;
        proxy_pass http://local-server-01:8801;
    }

    location ~^\/server-02$ {
        proxy_set_header X-test-header test-value;
        proxy_pass http://local-server-02:8802;
    }
    
    location ~^\/api\/v1\/server-01\/(?<path>.*)$ {
        proxy_set_header X-test-header test-value;
        proxy_pass http://local-server-01:8801/$path;
    }

    location ~^\/api\/v1\/server-02\/(?<path>.*)$ {
        proxy_set_header X-test-header test-value;
        proxy_pass http://local-server-02:8802/$path;
    }

    location ~^\/api\/v2\/server-(?<serverId>\d\d)\/(?<path>.*)$ {
        proxy_set_header X-test-header test-value;
        proxy_pass http://local-server-$serverId:88$serverId/$path;
    }
}
```

## Result
### location ~^\/server-01$
![](./images/postman_server01.png)


### location ~^\/server-02$
![](./images/postman_server02.png)


### location ~^\/api\/v1\/server-01\/(?<path>.*)$
![](./images/postman_api_v1_server01.png)


### location ~^\/api\/v1\/server-02\/(?<path>.*)$
![](./images/postman_api_v1_server02.png)


### location ~^\/api\/v2\/server-(?<serverId>\d\d)\/(?<path>.*)$
1. request to server01:
![](./images/postman_api_v2_server01.png)


2. request to server02:
![](./images/postman_api_v2_server02.png)
