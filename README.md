# nginx proxy
[![pipeline status](/badges/master/pipeline.svg)](/commits/master)


## Diagramme simplifié de l'environnement de test
![](./doc/regexp_proxy/images/arch_solution.png)

## Diagramme de l'architecture de la solution
![](./doc/provisionning/images/full_solution_arch.png)


## Ajout d'un header
- [Ajout d'un Header personnalisé dans le requête](./doc/append_header/README.md#ajout-dun-header-personnalisé-dans-le-requête)
	- [Environnement de test](./doc/append_header/README.md#environnement-de-test)
	- [Golang server](./doc/append_header/README.md#golang-server)
		- [Code](./doc/append_header/README.md#code)
		- [Résultat](./doc/append_header/README.md#résultat)
	- [Config de nginx](./doc/append_header/README.md#config-de-nginx)
		- [Commande Docker](./doc/append_header/README.md#commande-docker)
		- [Explication des configuration de proxy](./doc/append_header/README.md#explication-des-configuration-de-proxy)
		- [Résultat](./doc/append_header/README.md#résultat-1)


## RegExp proxy
- [proxy_pass avec des expressions régulieres](./doc/regexp_proxy/README.md#proxy_pass-avec-des-expressions-régulieres)
  - [Environnement de test](./doc/regexp_proxy/README.md#environnement-de-test)
  - [Config sur la machine hôte](./doc/regexp_proxy/README.md#config-sur-la-machine-hôte)
  - [Les serveurs golang](./doc/regexp_proxy/README.md#les-serveurs-golang)
  - [nginx config](./doc/regexp_proxy/README.md#nginx-config)
    - [nginx Dockerfile](./doc/regexp_proxy/README.md#nginx-dockerfile)
    - [./conf/default.conf](./doc/regexp_proxy/README.md#confdefaultconf)
  - [Résultat](./doc/regexp_proxy/README.md#résultat)
    - [Expressions régulieres basiques](./doc/regexp_proxy/README.md#expressions-régulieres-basiques)
      - [location ~^\/server-01$](./doc/regexp_proxy/README.md#location-server-01)
      - [location ~^\/server-02$](./doc/regexp_proxy/README.md#location-server-02)
    - [Expressions régulieres avec capture d'un bloc](./doc/regexp_proxy/README.md#expressions-régulieres-avec-capture-dun-bloc)
      - [location ~^\/api\/v1\/server-01\/(?<path>.*)$](./doc/regexp_proxy/README.md#location-apiv1server-01path)
      - [location ~^\/api\/v1\/server-02\/(?<path>.*)$](./doc/regexp_proxy/README.md#location-apiv1server-02path)
    - [Expressions régulieres avec capture de plusieurs bloc](./doc/regexp_proxy/README.md#expressions-régulieres-avec-capture-de-plusieurs-bloc)
      - [location ~^\/api\/v2\/server-(?<serverId>\d\d)\/(?<path>.*)$](./doc/regexp_proxy/README.md#location-apiv2server-serveridddpath)


## Demo avec AWS EC2
- [Provisonning](./doc/provisionning/README.md#provisonning)
  - [Architecture de la solution](./doc/provisionning/README.md#architecture-de-la-solution)
  - [Environnement](./doc/provisionning/README.md#environnement)
  - [Terraform](./doc/provisionning/README.md#terraform)
    - [Requirements](./doc/provisionning/README.md#requirements)
    - [Commandes](./doc/provisionning/README.md#commandes)
      - [terraform init](./doc/provisionning/README.md#terraform-init)
      - [terraform plan](./doc/provisionning/README.md#terraform-plan)
      - [terraform apply](./doc/provisionning/README.md#terraform-apply)
  - [Ansible](./doc/provisionning/README.md#ansible)
    - [playbook tasks](./doc/provisionning/README.md#playbook-tasks)
    - [output](./doc/provisionning/README.md#output)
